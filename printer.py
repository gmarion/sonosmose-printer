"""
    @author Guilhem Marion
    Automatically sends new images from a Instagram hashtag feed to a printer
"""

from instagram.client import InstagramAPI
import time
import cups
import sys

access_token = "2164050962.1a433a2.d90ea2c006a94562addb102581272f3c"
client_secret = "6b23a062162e47cb9e738416fa7b623c"
tag = "sonosmose"
sleep_time = 15

def send_to_printer(connection,printer,pictures):
    """ Prints on a cups connection, on a specified printer, the specified medias """
    print("Printing " + str(len(pictures)) + " pictures")
    fileNames = [m.get_standard_resolution_url() for m in pictures]
    conn.printFiles(printer,fileNames,"Sonosmose Printer Job",{})

def main():
    """ Run the whole program """

    # Make sure we have a correct access token before using the api
    api = InstagramAPI(access_token=access_token, client_secret=client_secret)

    #Initialize connection to printer
    conn = cups.Connection()
    printers = conn.getPrinters()
    if len(printers) == 0:
        print("Error : no printer detected !")
        sys.exit(1)
    else:
        print("Printing on printer : ",printers.keys()[0])
        printer = printers[0]

    # Beware, when using pagination, that you stop somewhere...
    res, next_ = api.tag_recent_media(1,tag_name=tag)
    last_id = res[0].id
    print("Last id : " + last_id)

    keep_polling = True
    while keep_polling:
        new_pictures = []
        res, next_ = api.tag_recent_media(tag_name=tag)


        keep_paginating = True
        while keep_paginating:
            # See whether we have found the newest id from last time
            # since min_tag_id doesn't seem to work with instagram
            for m in res:
                if m.id == last_id:
                    keep_paginating = False
                    break
                else:
                    # Only add the media to the printing queue if it's an image
                    if m.type == 'image':
                        new_pictures.append(m)
                        last_id = m.id

            # If need be, paginate to add more recent results to the new_pictures
            if keep_paginating and next_:
                more_res, next_ = api.user_follows(with_next_url=next_)
                res.extend(more_res)
            else:
                keep_paginating = False

        # If there are pictures to print, print them.
        send_to_printer(conn,printer,new_pictures)
        new_pictures.clear()
        time.sleep(sleep_time)

if __name__ == '__main__':
    main()
